import express from "express";

// express engines
import "reflect-metadata";
import * as bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import mustacheExpress from 'mustache-express';

// native modules
import path from 'path';

// my deps
import router from './src/router';

const app = express()
app.engine('mustache', mustacheExpress())
app.set('view engine', 'mustache');
app.set('views', path.resolve('src/assets'));
app.use(cookieParser())
app.use(bodyParser.urlencoded())
app.use(express.static('src/assets'))
app.use('/', router);

app.listen(3000, () => console.log('app listening on port 3000!'))
