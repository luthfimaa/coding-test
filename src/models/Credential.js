import { sequelize } from './base';
import { Sequelize } from 'sequelize';

const Credential = sequelize.define('credential', {
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
});

export default Credential;
