import { sequelize } from './base';
import { Sequelize } from 'sequelize';

const Customer = sequelize.define('customer', {
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
}, {underscored: true});

export default Customer;
