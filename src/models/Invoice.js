import { sequelize } from './base';
import { Sequelize } from 'sequelize';
import Customer from './Customer';

const Invoice = sequelize.define('invoice', {
  total_price: {
    type: Sequelize.INTEGER,
  },
  firstname: {
    type: Sequelize.STRING,
  },
  lastname: {
    type: Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING,
  },
  telephone: {
    type: Sequelize.STRING,
  },
  fax: {
    type: Sequelize.STRING,
  },
  company: {
    type: Sequelize.STRING,
  },
  address_1: {
    type: Sequelize.STRING,
  },
  address_2: {
    type: Sequelize.STRING,
  },
  city: {
    type: Sequelize.STRING,
  },
  postcode: {
    type: Sequelize.STRING,
  },
  country_id: {
    type: Sequelize.INTEGER,
  },
  zone_id: {
    type: Sequelize.INTEGER,
  },
  shipping_address: {
    type: Sequelize.STRING,
  },
  shipping_method: {
    type: Sequelize.STRING,
  },
  payment_method: {
    type: Sequelize.STRING,
  },
  coupon: {
    type: Sequelize.STRING,
  },
  voucher: {
    type: Sequelize.STRING,
  },
  comments: {
    type: Sequelize.STRING,
  },
}, {underscored: true});
Invoice.belongsTo(Customer)

export default Invoice;
