import { sequelize } from './base';
import { Sequelize } from 'sequelize';
import Product from './Product';
import Customer from './Customer';

const CartItem = sequelize.define('cart_item', {
  active: {
    type: Sequelize.BOOLEAN
  },
  quantity: {
    type: Sequelize.INTEGER
  },
  color: {
    type: Sequelize.INTEGER
  }
}, {underscored: true})
CartItem.belongsTo(Product);
CartItem.belongsTo(Customer);

export default CartItem;
