import { sequelize } from './base';
import { Sequelize } from 'sequelize';

const Product = sequelize.define('product', {
  name: {
    type: Sequelize.STRING,
  },
  price: {
    type: Sequelize.INTEGER,
  },
  description: {
    type: Sequelize.STRING,
  },
  category: {
    type: Sequelize.STRING,
  },
  image_uri: {
    type: Sequelize.STRING,
  },
}, { underscored: true });

export default Product;
