import path from 'path';
import Credential from '../models/Credential';

const assets = path.resolve('src/assets')

export async function auth(req, res, next){
  const { email, password } = req.body;
  const credential = await Credential.findOne({where: {email}})

  if(password === credential.password){
    res.cookie('user_id', credential.id)
    next()
  }
  else{
    res.send('Password did not matched')
  }
}


export async function isAuthenticated(req, res, next){
  const cookie = req.cookies.user_id;
  if(cookie !== undefined) next()
  else res.render('login');
}
