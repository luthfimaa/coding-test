import CartItem from '../models/CartItem';
import Customer from '../models/Customer';
import Product from '../models/Product';
import Invoice from '../models/Invoice';

export async function addToCart(req, res, next){
  const customer_id = req.cookies.user_id;
  const {product_id, quantity, color} = req.body;

  await CartItem.sync();
  CartItem.create({
    active: true,
    quantity,
    color,
    product_id,
    customer_id
  });
  next()
}

function getPrices(items){
  const subTotal = items.reduce((acc, val) => (acc + val.totalPrice), 0)
  const ecoTax = parseInt(subTotal * 0.1);
  const total = subTotal + ecoTax;
  return {subTotal, ecoTax, total}
}

export async function checkout(req, res, next){
  const customer_id = req.cookies.user_id;

  const items = await getCartItemsWithDetail(customer_id);
  const {subTotal, ecoTax, total} = getPrices(items);

  await Invoice.sync();
  Invoice.create({
    customer_id,
    total_price: total,
    ...req.body
  })

  await CartItem.sync();
  CartItem.update(
    { active: false, },
    { where: {customer_id}})

  res.render('purchase-completed');
}

export async function showCheckout(req, res, next){
  const customer_id = req.cookies.user_id;
  const items = await getCartItemsWithDetail(customer_id);
  const {subTotal, ecoTax, total} = getPrices(items);
  res.render('checkout', {items, subTotal, ecoTax, total});
}


export async function showCartDetail(req, res, next){
  const customer_id = req.cookies.user_id;
  const items = await getCartItemsWithDetail(customer_id);
  const {subTotal, ecoTax, total} = getPrices(items);
  res.render('cart', {items, subTotal, ecoTax, total});
}

// for testing purpose
//export async function dummyCreateInvoice(req, res, next){
//  const customer = await Customer.findById(5);
//  const customerId = customer.id
//  await Invoice.sync();
//  Invoice.create({total_price: 5000, customerId});
//}

//export async function dummyAddToCart(req, res, next){
//  CartItem.sync();
//  CartItem.create({
//    active: true,
//    quantity: 1,
//    color: 1,
//    product_id: 26,
//    customer_id: 5
//  })
//}

function compute_total_price(items, tax_rate){
  const gross = items.reduce(item => (item.price * item.quantity))
  return parseInt(gross * (1-tax_rate));
}


async function getCartItemsWithDetail(customer_id){
  await CartItem.sync()
  const itemObjects = await CartItem.findAll({where: {active: true, customer_id}});
  const items = itemObjects.map(item => item.dataValues);

  const productObjects = await Promise.all(items.map((item) => Product.findById(item.product_id)));
  const products = productObjects.map(product => product.dataValues);

  let displayItems = [];
  for(var i = 0; i < products.length; i++){
    let cartItem = items[i];
    let product = products[i];
    let item = {
      name: product.name,
      model: 'Lorem Ipsum',
      price: product.price,
      quantity: cartItem.quantity,
      totalPrice: product.price * cartItem.quantity
    }
    displayItems.push(item);
  }
  return displayItems;
}
