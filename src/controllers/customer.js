import Customer from '../models/Customer';
import Credential from '../models/Credential';

export async function register(req, res, next){
  const { firstname, lastname, email, password } = req.body;
  await Credential.sync()
  Credential.create({email, password})

  await Customer.sync()
  const c = await Customer.create({
    firstName: firstname,
    lastName: lastname,
    email,
  })
  res.cookie('user_id', c.id)
  next()
}
