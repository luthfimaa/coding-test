import Product from '../models/Product';
import path from 'path';

const assets = path.resolve('src/assets')

export async function showHome(req, res, next){
  await Product.sync();
  const productObjects = await Product.findAll();
  const bestSellers = productObjects.map(p => p.dataValues);
  res.render('index', {bestSellers})

}

export async function showDetail(req, res, next){
  const id = req.params.id;
  const product = await Product.findById(id)
  const {name, price, description, image_uri} = product;
  const afterTax = parseInt(price * 1.1)  // 10% tax
  res.render('product-detail', {id, name, price, description, image_uri, afterTax})
}

export async function create_all(req, res){
  const products = [
    {name: 'Corsair Raptor', price: 922000, description: 'Lorem ipsum dolor sit amet', category: 'accessory', image_uri: 'image/product/accessories/1.jpg'},
    {name: 'Logitech G213', price: 580000, description: 'Lorem ipsum dolor sit amet', category: 'accessory', image_uri: 'image/product/accesories/2.jpg'},
    {name: 'ASUS Notebook E202SA', price: 3650000, description: 'Lorem ipsum dolor sit amet', category: 'laptop', image_uri: 'image/product/laptop/1.jpg'},
    {name: 'ASUS Notebook X455LJ', price: 6099000, description: 'Lorem ipsum dolor sit amet', category: 'laptop', image_uri: 'image/product/laptop/2.jpg'},
    {name: 'Apple Iphone 7 256GB - Jet Black', price: 15599000, description: 'Lorem ipsum dolor sit amet', category: 'smartphone', image_uri: 'image/product/smartphone/1.jpg'},
    {name: 'Apple Iphone 7 Plus 256GB - Rose Gold', price: 12369000, description: 'Lorem ipsum dolor sit amet', category: 'smartphone', image_uri: 'image/product/smartphone/2.jpg'},
  ]

  await Product.sync();
  products.forEach(p => {
    Product.create({
      name: p.name,
      price: p.price,
      description: p.description,
      category: p.category,
      image_uri: p.image_uri
    });
  });
}
