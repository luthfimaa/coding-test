import * as express from 'express';
import { register } from './controllers/customer';
import { auth, isAuthenticated } from './controllers/auth';
import { create_all, showDetail, showHome } from './controllers/product';
import { addToCart, checkout, showCartDetail, showCheckout} from './controllers/cart';
import path from 'path';

const router = express.Router();
const assets = path.resolve('src/assets')

//router.get('/', function (req, res) {
//  res.render('index')
//})
router.get('/', showHome);

router.get('/register', (req, res) => { 
  res.render('register')})

router.get('/login', (req, res) => { 
  res.render('login')})

router.get('/products/:id', showDetail);

// ----------- handlers ---------------

router.post('/register-handler', register);
router.post('/login-handler', auth)

// ---------- require authentication -------

router.post('/add-to-cart-handler', isAuthenticated, addToCart)
router.get('/cart', isAuthenticated, showCartDetail)
router.get('/checkout', isAuthenticated, showCheckout);
router.post('/checkout-handler', isAuthenticated, checkout)

// ----------- helpers - don't use in production ---------
router.get('/create-all-products', create_all);

router.post('/*', (req, res) => { // redirect to home after successful auth
  res.redirect('/')
})

export default router;
